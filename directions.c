//
//  directions.c
//  toyrobotsimulator
//
//  Created by Saujan Thapa on 24/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#include <string.h>

#include "directions.h"

static const int ROBOT_DIRECTION_LENGTH = 4;

static const char *ROBOT_DIRECTION[] = {
        "NORTH",
        "SOUTH",
        "EAST",
        "WEST"
};

direction_id CreateDirection() {
    direction_id id = malloc(sizeof(direction_t));

    if (id != NULL) {
        id->east = false;
        id->north = false;
        id->south = false;
        id->west = false;
        memset(id->name, '\0', sizeof(id->name));
    }
    return id;
}


void MOVE(direction_id id, tabletop_id t, int *x, int *y) {
    int new_x = *x;
    int new_y= *y;
    if (id->south) {
        new_y = new_y - 1;
    } else if (id->north) {
        new_y = new_y + 1;
    } else if (id->east) {
        new_x = new_x + 1;
    } else if (id->west) {
        new_x = new_x - 1;
    }
    if (new_x >= 0 && new_x < t->max_x && new_y >= 0 && new_y < t->max_y) {
        *x = new_x;
        *y = new_y;
    }
}

void LEFT(direction_id id) {
    if (id->south) {
        id->south = false;
        id->east = true;
    } else if (id->north) {
        id->north = false;
        id->west = true;
    } else if (id->east) {
        id->east = false;
        id->north = true;
    } else if (id->west) {
        id->west = false;
        id->south = true;
    }
}

void RIGHT(direction_id id) {
    if (id->south) {
        id->south = false;
        id->west = true;
    } else if (id->north) {
        id->north = false;
        id->east = true;
    } else if (id->east) {
        id->east = false;
        id->south = true;
    } else if (id->west) {
        id->west = false;
        id->north = true;
    }
}

void ScanDirection(direction_id id) {
    if (id != NULL) {
        if (id->west) {
            strcpy(id->name,"WEST");
        }
        else if (id->east) {
            strcpy(id->name, "EAST");
        }
        else if (id->south) {
            strcpy(id->name, "SOUTH");
        }
        else if (id->north) {
            strcpy(id->name, "NORTH");
        }
    }
}

bool ParseDirection(direction_id id, char * str) {
    if (strlen(str) != 0) {
        for (int i = 0; i < ROBOT_DIRECTION_LENGTH; i++) {
            if (strcmp(str, ROBOT_DIRECTION[i]) == 0) {
                switch (i) {
                    case 0:
                        id->north = true;
                        id->south = false;
                        id->east = false;
                        id->west = false;
                        return true;
                    case 1:
                        id->south = true;
                        id->north = false;
                        id->east = false;
                        id->west = false;
                        return true;
                    case 2:
                        id->east = true;
                        id->south = false;
                        id->north = false;
                        id->west = false;
                        return true;
                    case 3:
                        id->west = true;
                        id->south = false;
                        id->north = false;
                        id->east = false;
                        return true;
                }
            }
        }
    }
    return false;
}

bool ResetDirection(direction_id id) {
    if (id != NULL) {
        id->east = false;
        id->north = false;
        id->south = false;
        id->west = false;
        memset(id->name, '\0', sizeof(id->name));
        return true;
    }
    return false;
}

void DestroyDirection(direction_id id) {
    if (id != NULL) {
        free(id);
    }
}
