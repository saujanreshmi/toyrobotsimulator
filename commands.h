//
//  commands.h
//  toyrobotsimulator
//
//  Created by Saujan Thapa on 24/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#ifndef commands_h
#define commands_h

#include <stdbool.h>

#include "directions.h"
#include "tabletops.h"


/*
 * Data structure used to manage a robots' commands.
 *
 * Members:
 *      command_id: The unique Id of the command.
 *      SINGLE_COMMAND: Boolean variable to determine if command was single word command.
 *      PLACE: Boolean variable to hold 'PLACE' command.
 *      MOVE: Boolean variable to hold 'MOVE' command.
 *      LEFT: Boolean variable to hold 'LEFT' command.
 *      RIGHT: Boolean variable to hold 'RIGHT' command.
 *      REPORT: Boolean variable to hold 'REPORT' command.
 *      END: Boolean variable to hold 'END' command.
 *      X_POS: X coordinate on the tabletop where robot should be placed.
 *      Y_POS: Y coordinate on the tabletop where robot should be placed.
 *      DIRECTION: Facing direction in the 'PLACE' command.
 */
typedef struct command{
    bool SINGLE_COMMAND;
    bool PLACE;
    bool MOVE;
    bool LEFT;
    bool RIGHT;
    bool REPORT;
    bool END;
    int X_POS;
    int Y_POS;
    direction_id DIRECTION;
}command_t;

typedef command_t * command_id;

/*
 * CreateCommand:
 *
 * Initialises a command to default variable.
 *
 * Input:
 *      N\A
 *
 * Output:
 *      Returns the address of an initialised command object.
 */
command_id CreateCommand();

/*
 * ParseCommand:
 *
 * Parses the string into command object.
 *
 * Input:
 *      id: Address of declared command.
 *      str: raw command in the format of string.
 *
 * Output:
 *      Returns true if uccessful and false otherwise.
 */
bool ParseCommand(command_id id, char * str);

/*
 * ValidateCommand:
 *
 * Checks if command object holds valid command for the tabletop.
 *
 * Input:
 *      id: Address of a declared command.
 *      t: Address of a declared tabletop.
 *
 * Output:
 *      Returns true if successful and false otherwise.
 */
bool ValidateCommand(command_id id, tabletop_id t);

/*
 * ResetCommand:
 *
 * Resets the command.
 *
 * Input:
 *      id: Address of a declared command.
 *
 * Output:
 *      Returns true if successful and false otherwise.
 */
bool ResetCommand(command_id id);

/*
 * Releases the memory resources by the command.
 */
void DestroyCommand(command_id id);

#endif /* commands_h */
