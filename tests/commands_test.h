//
//  commands_test.h
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//
#ifndef commandstest_h
#define commandstest_h

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_CreateCommand(void **state);

void test_ParseCommand(void **state);

void test_ValidateCommand(void **state);

void test_ResetCommand(void **state);

#endif