//
//  directions_test.c
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//
#include <string.h>

#include "directions_test.h"

#include "../directions.h"

void test_CreateDirection(void **state) {
    direction_id direction = CreateDirection();
    assert_false(direction->north);
    assert_false(direction->south);
    assert_false(direction->east);
    assert_false(direction->west);
    assert_int_equal(strlen(direction->name),0);
    DestroyDirection(direction);
}

void test_MOVE(void **state) {
    tabletop_id t1 = CreateTabletop(0);
    direction_id d1 = CreateDirection();
    int x=0,y=0;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,0);

    d1->north = true;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,1);

    d1->north = false;
    d1->east = true;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,1);
    assert_int_equal(y,1);

    d1->east = false;
    d1->south = true;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,1);
    assert_int_equal(y,0);

    d1->south = false;
    d1->west = true;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,0);

    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,0);

    d1->north = true;
    d1->west = false;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,1);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,2);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,3);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,4);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,4);

    d1->north = false;
    d1->east = true;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,1);
    assert_int_equal(y,4);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,2);
    assert_int_equal(y,4);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,3);
    assert_int_equal(y,4);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,4);
    assert_int_equal(y,4);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,4);
    assert_int_equal(y,4);

    d1->east = false;
    d1->south = true;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,4);
    assert_int_equal(y,3);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,4);
    assert_int_equal(y,2);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,4);
    assert_int_equal(y,1);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,4);
    assert_int_equal(y,0);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,4);
    assert_int_equal(y,0);

    d1->south = false;
    d1->west = true;
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,3);
    assert_int_equal(y,0);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,2);
    assert_int_equal(y,0);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,1);
    assert_int_equal(y,0);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,0);
    MOVE(d1, t1, &x, &y);
    assert_int_equal(x,0);
    assert_int_equal(y,0);

    DestroyTabletop(t1);
    DestroyDirection(d1);
}

void test_LEFT(void **state) {
    direction_id d1 = CreateDirection();
    assert_false(d1->west);
    assert_false(d1->east);
    assert_false(d1->north);
    assert_false(d1->south);
    d1->north = true;
    LEFT(d1);
    assert_true(d1->west);
    assert_false(d1->east);
    assert_false(d1->north);
    assert_false(d1->south);
    LEFT(d1);
    assert_true(d1->south);
    assert_false(d1->east);
    assert_false(d1->north);
    assert_false(d1->west);
    LEFT(d1);
    assert_true(d1->east);
    assert_false(d1->south);
    assert_false(d1->north);
    assert_false(d1->west);
    LEFT(d1);
    assert_true(d1->north);
    assert_false(d1->east);
    assert_false(d1->south);
    assert_false(d1->west);
    DestroyDirection(d1);
}

void test_RIGHT(void **state) {
    direction_id d1 = CreateDirection();
    assert_false(d1->west);
    assert_false(d1->east);
    assert_false(d1->north);
    assert_false(d1->south);
    d1->north = true;
    RIGHT(d1);
    assert_true(d1->east);
    assert_false(d1->west);
    assert_false(d1->north);
    assert_false(d1->south);
    RIGHT(d1);
    assert_true(d1->south);
    assert_false(d1->west);
    assert_false(d1->north);
    assert_false(d1->east);
    RIGHT(d1);
    assert_true(d1->west);
    assert_false(d1->east);
    assert_false(d1->north);
    assert_false(d1->south);
    RIGHT(d1);
    assert_true(d1->north);
    assert_false(d1->west);
    assert_false(d1->east);
    assert_false(d1->south);
    DestroyDirection(d1);
}

void test_ScanDirection(void **state) {
    direction_id d1 = CreateDirection();
    d1->west = true;
    ScanDirection(d1);
    assert_string_equal("WEST",d1->name);
    d1->west = false;
    d1->east = true;
    ScanDirection(d1);
    assert_string_equal("EAST",d1->name);
    d1->north = true;
    d1->east = false;
    ScanDirection(d1);
    assert_string_equal("NORTH",d1->name);
    d1->north = false;
    d1->south = true;
    ScanDirection(d1);
    assert_string_equal("SOUTH",d1->name);
    d1->south = false;
    d1->east = true;
    ScanDirection(d1);
    assert_string_equal("EAST",d1->name);
    d1->north = true;
    d1->east = false;
    ScanDirection(d1);
    assert_string_equal("NORTH",d1->name);
    d1->north = false;
    d1->west = true;
    ScanDirection(d1);
    assert_string_equal("WEST",d1->name);
    DestroyDirection(d1);
}

void test_ParseDirection(void **state) {
    direction_id d1 = CreateDirection();
    assert_true(ParseDirection(d1, "NORTH"));
    assert_true(d1->north);
    assert_false(d1->east);
    assert_false(d1->west);
    assert_false(d1->south);
    assert_true(ParseDirection(d1, "SOUTH"));
    assert_true(d1->south);
    assert_false(d1->north);
    assert_false(d1->east);
    assert_false(d1->west);
    assert_true(ParseDirection(d1, "EAST"));
    assert_true(d1->east);
    assert_false(d1->north);
    assert_false(d1->south);
    assert_false(d1->west);
    assert_true(ParseDirection(d1, "WEST"));
    assert_true(d1->west);
    assert_false(d1->north);
    assert_false(d1->east);
    assert_false(d1->south);
    assert_false(ParseDirection(d1, "west"));
    assert_true(d1->west);
    assert_false(d1->north);
    assert_false(d1->east);
    assert_false(d1->south);
    assert_false(ParseDirection(d1, "east"));
    assert_true(d1->west);
    assert_false(d1->north);
    assert_false(d1->east);
    assert_false(d1->south);
    assert_false(ParseDirection(d1, "south"));
    assert_true(d1->west);
    assert_false(d1->north);
    assert_false(d1->east);
    assert_false(d1->south);
    assert_false(ParseDirection(d1, "north"));
    assert_true(d1->west);
    assert_false(d1->north);
    assert_false(d1->east);
    assert_false(d1->south);
    assert_false(ParseDirection(d1, ""));
    assert_true(d1->west);
    assert_false(d1->north);
    assert_false(d1->east);
    assert_false(d1->south);
    assert_false(ParseDirection(d1, "sdfasdfasf"));
    assert_true(d1->west);
    assert_false(d1->north);
    assert_false(d1->east);
    assert_false(d1->south);

    DestroyDirection(d1);
}

void test_ResetDirection(void **state) {
    direction_id d1 = CreateDirection();
    assert_true(ParseDirection(d1, "NORTH"));
    assert_true(d1->north);
    ScanDirection(d1);
    assert_string_equal("NORTH",d1->name);
    assert_false(d1->east);
    assert_false(d1->west);
    assert_false(d1->south);
    ResetDirection(d1);
    assert_string_equal("",d1->name);
    assert_false(d1->east);
    assert_false(d1->west);
    assert_false(d1->south);
    assert_false(d1->north);

    DestroyDirection(d1);
}

