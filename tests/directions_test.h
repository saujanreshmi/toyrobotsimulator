//
//  directions_test.h
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//
#ifndef directionstest_h
#define directionstest_h

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_CreateDirection(void **state);

void test_MOVE(void **state);

void test_LEFT(void **state);

void test_RIGHT(void **state);

void test_ScanDirection(void **state);

void test_ParseDirection(void **state);

void test_ResetDirection(void **state);
#endif