//
//  commands_test.c
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#include "commands_test.h"

#include "../commands.h"

void test_CreateCommand(void **state) {
    command_id command = CreateCommand();
    assert_false(command->SINGLE_COMMAND);
    assert_false(command->RIGHT);
    assert_false(command->LEFT);
    assert_false(command->MOVE);
    assert_false(command->PLACE);
    assert_int_equal(command->Y_POS, 0);
    assert_int_equal(command->X_POS, 0);
    assert_non_null(command->DIRECTION);
    DestroyCommand(command);
}

void test_ParseCommand(void **state) {
    command_id id = CreateCommand();
    assert_true(ParseCommand(id, "MOVE"));
    assert_true(ParseCommand(id, "LEFT"));
    assert_true(ParseCommand(id, "RIGHT"));
    assert_true(ParseCommand(id, "REPORT"));
    assert_true(ParseCommand(id, "END"));
    assert_true(ParseCommand(id, "PLACE 0,0,NORTH"));
    assert_true(ParseCommand(id, "PLACE 0,0,SOUTH"));
    assert_true(ParseCommand(id, "PLACE 0,0,EAST"));
    assert_true(ParseCommand(id, "PLACE 0,0,WEST"));
    assert_true(ParseCommand(id,"PLACE 255,255,NORTH"));
    assert_true(ParseCommand(id,"PLACE 255,255,SOUTH"));
    assert_true(ParseCommand(id,"PLACE 255,255,EAST"));
    assert_true(ParseCommand(id,"PLACE 255,255,WEST"));
    assert_true(ParseCommand(id,"PLACE 999,999,NORTH"));
    assert_true(ParseCommand(id,"PLACE 999,999,SOUTH"));
    assert_true(ParseCommand(id,"PLACE 999,999,EAST"));
    assert_true(ParseCommand(id,"PLACE 999,999,WEST"));

    assert_false(ParseCommand(id,"PLACE 1000,999,NORTH"));
    assert_false(ParseCommand(id,"PLACE 999,1000,SOUTH"));
    assert_false(ParseCommand(id,"PLACE 1000,1000,EAST"));
    assert_false(ParseCommand(id,"PLACE 9999,9999,WEST"));

    assert_false(ParseCommand(id, "MOVE "));
    assert_false(ParseCommand(id, " MOVE"));
    assert_false(ParseCommand(id, "MOVE 0"));
    assert_false(ParseCommand(id, "MOV"));

    assert_false(ParseCommand(id, "LEFT  "));
    assert_false(ParseCommand(id, "  LEFT"));
    assert_false(ParseCommand(id, "LEFT 0"));
    assert_false(ParseCommand(id, "LEFTT"));

    assert_false(ParseCommand(id, "RIGHT "));
    assert_false(ParseCommand(id, " RIGHT"));
    assert_false(ParseCommand(id, "RIGHT 0,0"));
    assert_false(ParseCommand(id, "RIGHT 0,0,NORTH"));
    assert_false(ParseCommand(id, " RIGHT 0,0,NORTH"));

    assert_false(ParseCommand(id, "REPORT  "));
    assert_false(ParseCommand(id, "  REPORT"));
    assert_false(ParseCommand(id, "REPORT 0,0"));
    assert_false(ParseCommand(id, "REPORT 0,0,NORTH"));
    assert_false(ParseCommand(id, " REPORT 0,0,NORTH"));

    assert_false(ParseCommand(id, "PLACE"));
    assert_false(ParseCommand(id, "PLACE  0,0,NORTH"));
    assert_false(ParseCommand(id, "PLACE 0 ,0,NORTH"));
    assert_false(ParseCommand(id, "PLACE 0, 0, NORTH"));
    assert_false(ParseCommand(id, "PLACE 0,0 ,NORTH"));
    assert_false(ParseCommand(id, "PLACE 0,0, NORTH"));
    assert_false(ParseCommand(id, "PLACE 0,0,NORTH "));
    assert_false(ParseCommand(id,"PLACE,0 0,NORTH"));
    assert_false(ParseCommand(id,"PLACE 25a,23a,NORTH"));
    assert_false(ParseCommand(id,"PLACE 255,2333,NORTH"));
    assert_false(ParseCommand(id, "PLACE -1,-1,NORTH"));
    assert_false(ParseCommand(id, "PLACE -1,-1,SOUTH"));
    assert_false(ParseCommand(id, "PLACE -1,-1,EAST"));
    assert_false(ParseCommand(id, "PLACE -1,-1,WEST"));

    assert_false(ParseCommand(id, ""));
    assert_false(ParseCommand(id, "move"));
    assert_false(ParseCommand(id, "left"));
    assert_false(ParseCommand(id, "right"));
    assert_false(ParseCommand(id, "report"));
    assert_false(ParseCommand(id, "place"));
    assert_false(ParseCommand(id, "place 0,0,NORTH sdfasfdsaf"));
    DestroyCommand(id);
}

void test_ValidateCommand(void **state) {
    tabletop_id t = CreateTabletop(0);
    command_id c = CreateCommand();
    ParseCommand(c, "MOVE");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "LEFT");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "RIGHT");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "REPORT");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "END");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,0,NORTH");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,0,SOUTH");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,0,EAST");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,0,WEST");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 4,4,NORTH");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 4,4,SOUTH");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 4,4,EAST");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 4,4,WEST");
    assert_true(ValidateCommand(c,t));

    ParseCommand(c,"PLACE 5,5,NORTH");
    assert_false(ValidateCommand(c,t));
    ParseCommand(c,"PLACE 5,5,SOUTH");
    assert_false(ValidateCommand(c,t));
    ParseCommand(c,"PLACE 5,5,EAST");
    assert_false(ValidateCommand(c,t));
    ParseCommand(c,"PLACE 5,5,WEST");
    assert_false(ValidateCommand(c,t));

    ResetTabletop(t,255);
    ParseCommand(c, "MOVE");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "LEFT");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "RIGHT");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "REPORT");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "END");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,0,NORTH");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,0,SOUTH");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,0,EAST");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,0,WEST");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 254,0,NORTH");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 254,0,SOUTH");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 0,254,EAST");
    assert_true(ValidateCommand(c,t));
    ParseCommand(c, "PLACE 254,254,WEST");
    assert_true(ValidateCommand(c,t));

    ParseCommand(c,"PLACE 255,255,NORTH");
    assert_false(ValidateCommand(c,t));
    ParseCommand(c,"PLACE 0,255,SOUTH");
    assert_false(ValidateCommand(c,t));
    ParseCommand(c,"PLACE 255,0,EAST");
    assert_false(ValidateCommand(c,t));
    ParseCommand(c,"PLACE 255,255,WEST");
    assert_false(ValidateCommand(c,t));

    DestroyTabletop(t);
    DestroyCommand(c);
}

void test_ResetCommand(void **state) {
    command_id command = CreateCommand();
    ParseCommand(command,"PLACE 5,5,SOUTH");
    ResetCommand(command);
    assert_false(command->SINGLE_COMMAND);
    assert_false(command->RIGHT);
    assert_false(command->LEFT);
    assert_false(command->MOVE);
    assert_false(command->PLACE);
    assert_int_equal(command->Y_POS, 0);
    assert_int_equal(command->X_POS, 0);
    assert_non_null(command->DIRECTION);
    DestroyCommand(command);
}