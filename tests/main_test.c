//
//  main_test.c
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "tabletops_test.h"
#include "directions_test.h"
#include "commands_test.h"
#include "robots_test.h"


int main(void)
{
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(test_CreateTabletop),
            cmocka_unit_test(test_ResetTabletop),
            cmocka_unit_test(test_CreateDirection),
            cmocka_unit_test(test_MOVE),
            cmocka_unit_test(test_LEFT),
            cmocka_unit_test(test_RIGHT),
            cmocka_unit_test(test_ScanDirection),
            cmocka_unit_test(test_ParseDirection),
            cmocka_unit_test(test_ResetDirection),
            cmocka_unit_test(test_CreateCommand),
            cmocka_unit_test(test_ParseCommand),
            cmocka_unit_test(test_ValidateCommand),
            cmocka_unit_test(test_ResetCommand),
            cmocka_unit_test(test_CreateRobot),
            cmocka_unit_test(test_ExecuteRobotCommand),
            cmocka_unit_test(test_ResetRobot),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}

