//
//  robots_test.h
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//
#ifndef robotstest_h
#define robotstest_h

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_CreateRobot(void **state);

void test_ExecuteRobotCommand(void **state);

void test_ResetRobot(void **state);
#endif