#Building and Running toyrobotsimulator unit tests


1. Open terminal and change to this location

2. Run 'make'
[this should create executable called 'tests']
if make command returns 'make: Nothing to be done for `all`.'
then run 'make clean' to clean the build files and run 'make' again

3. Run 'make run' or just run the executable from terminal