//
//  tabletops_test.c
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#include "tabletops_test.h"
#include "../tabletops.h"

void test_CreateTabletop(void **state) {
    tabletop_id id1 = CreateTabletop(0);
    assert_int_equal(id1->max_x, 5);
    assert_int_equal(id1->max_y, 5);
    tabletop_id id2 = CreateTabletop(256);
    assert_int_equal(id2->max_x, 5);
    assert_int_equal(id2->max_x, 5);
    tabletop_id id3 = CreateTabletop(255);
    assert_int_equal(id3->max_x, 255);
    assert_int_equal(id3->max_x, 255);
    tabletop_id id4 = CreateTabletop(20);
    assert_int_equal(id4->max_x, 20);
    assert_int_equal(id4->max_x, 20);
    tabletop_id id5 = CreateTabletop(-1);
    assert_int_equal(id5->max_x, 5);
    assert_int_equal(id5->max_x, 5);
    DestroyTabletop(id1);
    DestroyTabletop(id2);
    DestroyTabletop(id3);
    DestroyTabletop(id4);
    DestroyTabletop(id5);
}

void test_ResetTabletop(void **state) {
    tabletop_id id = CreateTabletop(0);
    assert_false(ResetTabletop(NULL, 0));
    assert_true(ResetTabletop(id, 5));
    assert_int_equal(id->max_x, 5);
    assert_int_equal(id->max_x, 5);
    assert_true(ResetTabletop(id, 255));
    assert_int_equal(id->max_x, 255);
    assert_int_equal(id->max_x, 255);
    assert_true(ResetTabletop(id, 20));
    assert_int_equal(id->max_x, 20);
    assert_int_equal(id->max_x, 20);
    assert_true(ResetTabletop(id, -5));
    assert_int_equal(id->max_x, 5);
    assert_int_equal(id->max_x, 5);
    DestroyTabletop(id);
}