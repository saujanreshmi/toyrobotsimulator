//
//  robots_test.c
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#include "robots_test.h"

#include "../robots.h"

void test_CreateRobot(void **state) {
    robot_id robot = CreateRobot();
    assert_int_equal(robot->x, -1);
    assert_int_equal(robot->y, -1);
    assert_non_null(robot->command);
    assert_non_null(robot->tabletop);
    assert_non_null(robot->direction);
    DestroyRobot(robot);
}

void test_ExecuteRobotCommand(void **state) {
    robot_id robot = CreateRobot();
    char feedback[50];
    assert_false(ExecuteRobotCommand(robot,"MOVE",feedback, 50));
    assert_false(ExecuteRobotCommand(robot,"LEFT",feedback, 50));
    assert_false(ExecuteRobotCommand(robot,"RIGHT",feedback, 50));
    assert_false(ExecuteRobotCommand(robot,"MOVE",feedback, 50));
    assert_false(ExecuteRobotCommand(robot,"REPORT",feedback, 50));
    assert_true(ExecuteRobotCommand(robot,"PLACE 1,2,EAST",feedback, 50));
    assert_true(ExecuteRobotCommand(robot,"MOVE",feedback, 50));
    assert_true(ExecuteRobotCommand(robot,"MOVE",feedback, 50));
    assert_true(ExecuteRobotCommand(robot,"LEFT",feedback, 50));
    assert_true(ExecuteRobotCommand(robot,"MOVE",feedback, 50));
    assert_true(ExecuteRobotCommand(robot,"REPORT",feedback, 50));
    assert_string_equal("Output: 3,3,NORTH",feedback);
    assert_true(ResetRobot(robot));
    assert_true(ExecuteRobotCommand(robot,"PLACE 0,0,NORTH",feedback,50));
    assert_true(ExecuteRobotCommand(robot,"MOVE",feedback, 50));
    assert_true(ExecuteRobotCommand(robot,"REPORT",feedback, 50));
    assert_string_equal("Output: 0,1,NORTH",feedback);
    assert_true(ResetRobot(robot,5));
    assert_true(ExecuteRobotCommand(robot,"PLACE 0,0,NORTH",feedback,50));
    assert_true(ExecuteRobotCommand(robot,"LEFT",feedback, 50));
    assert_true(ExecuteRobotCommand(robot,"REPORT",feedback, 50));
    assert_string_equal("Output: 0,0,WEST",feedback);
    //assert_true(ExecuteRobotCommand(robot,"END",feedback, 50));
    DestroyRobot(robot);
}

void test_ResetRobot(void **state) {
    robot_id robot = CreateRobot();
    char feedback[50];
    assert_true(ExecuteRobotCommand(robot,"PLACE 1,2,EAST",feedback, 50));
    assert_true(ResetRobot(robot));
    assert_int_equal(robot->x, -1);
    assert_int_equal(robot->y, -1);
    assert_false(robot->direction->north);
    assert_false(robot->direction->south);
    assert_false(robot->direction->east);
    assert_false(robot->direction->west);
    assert_int_equal(robot->tabletop->max_x, 5);
    assert_int_equal(robot->tabletop->max_y, 5);
    assert_false(robot->command->PLACE);
    assert_false(robot->command->END);
    assert_int_equal(robot->command->X_POS, 0);
    assert_int_equal(robot->command->Y_POS, 0);
    assert_true(ResetRobot(robot,255));
    assert_int_equal(robot->x, -1);
    assert_int_equal(robot->y, -1);
    assert_int_equal(robot->tabletop->max_x, 255);
    assert_int_equal(robot->tabletop->max_y, 255);
    DestroyRobot(robot);
}