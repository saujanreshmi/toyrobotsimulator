//
//  tabletops_test.h
//  toyrobotsimulator/tests
//
//  Created by Saujan Thapa on 25/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#ifndef tabletopstest_h
#define tabletopstest_h

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

void test_CreateTabletop(void **state);

void test_ResetTabletop(void **state);

#endif