#System Requirements:

Windows:
1. Cygwin terminal with following packages:
	a. gcc-g++ v7.4 
	b. make v4.2
	c. wget v1.19 [optional if need to run unit tests]
	d. cmake v3.6 [optional if need to run unit tests]

Linux:
(All linux machine should have mandatory tools required)
1. gcc compiler v7.4
2. gnu make v4.2
3. wget       [optional if need to run unit tests]
4. cmake v3.6 [optional if need to run unit tests]

Mac OS:
(Installing xcode should install mandatory tools required)
1. gcc compiler
2. gnu make
3. wget       [optional if need to run unit tests]
4. cmake v3.6 [optional if need to run unit tests]



#Building toyrobotsimulator library


1. Uncompress the provided file in your desired location

2. Open terminal and change to this location.

3. Run 'make'.
[this should create libtoyrobotsimulator.a file]
if make command returns 'make: Nothing to be done for `all`.'
then run 'make clean' to clean the build files and run 'make' again



#Using toyrobotsimulator library

Check the provided cliinterface for examples on how to use it.
This library provide three main functions:
1. CreateRobot() -> This function creates new instance of a toyrobot.
2. ExecuteRobotCommand() -> This function executes the provided robot command.
3. DestroyRobot() -> This function releases the resources used by the robot. 
[Check "robots.h" file for function details]
[Note: provided cliinterface program does not call DestroyRobot directecly however when END command does]




