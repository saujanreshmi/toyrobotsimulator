//
//  commands.c
//  toyrobotsimulator
//
//  Created by Saujan Thapa on 24/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#include <string.h>
#include <ctype.h>

#include "commands.h"

static const int ROBOT_COMMAND_LENGTH = 6;

static const char *ROBOT_COMMAND[] = {
        "PLACE",
        "MOVE",
        "LEFT",
        "RIGHT",
        "REPORT",
        "END"
};

command_id CreateCommand() {
    command_id id = malloc(sizeof(command_t));

    if (id != NULL){
        id->SINGLE_COMMAND = false;
        id->LEFT = false;
        id->RIGHT = false;
        id->MOVE = false;
        id->REPORT = false;
        id->PLACE = false;
        id->END = false;
        id->X_POS = 0;
        id->Y_POS = 0;
        id->DIRECTION = CreateDirection();
        if (id->DIRECTION == NULL) {
            return NULL;
        }
    }
    return id;
}

static bool validCommandFormat(char *cmd, int *type) {
    int cmdlen = strlen(cmd);
    if (!cmd || cmdlen>19) {
        return false;
    }
    int space=0;
    int comma=0;
    for (int i=0; i<cmdlen; i++) {
        if (cmd[i]==' '){
            space++;
        }else if (cmd[i] == ','){
            comma++;
        }
    }
    if (space == 1 && comma == 2){
        *type = 4;
        return true;
    } else if (space == 0 && comma == 0) {
        *type = 1;
        return true;
    }
    return false;
}

bool ParseCommand(command_id id, char * cmd) {
    if (ResetCommand(id)) {
        int cmdtype = 0;
        if (validCommandFormat(cmd, &cmdtype)) {
            char first_arg[7];
            char last_arg[6];
            char x_arg[4];
            char y_arg[4];
            memset(x_arg, '\0', 4);
            memset(y_arg, '\0', 4);
            memset(first_arg, '\0', 7);
            memset(last_arg, '\0', 6);
            switch (cmdtype) {
                case 1:
                    sscanf(cmd, "%s", first_arg);
                    break;
                case 4:
                    sscanf(cmd, "%s %[^,],%[^,],%[^\n\t]", first_arg, x_arg, y_arg, last_arg);
                    for (int i = 0; i < strlen(x_arg); i++) {
                        if (!isdigit(x_arg[i])) {
                            return false;
                        }
                    }
                    for (int i = 0; i < strlen(y_arg); i++) {
                        if (!isdigit(y_arg[i])) {
                            return false;
                        }
                    }

                    id->Y_POS = atoi(y_arg);
                    id->X_POS = atoi(x_arg);
                    break;
            }
            for (int i = 0; i < ROBOT_COMMAND_LENGTH; i++) {
                if (strcmp(first_arg, ROBOT_COMMAND[i]) == 0) {
                    switch (i) {
                        case 0:
                            id->PLACE = true;
                            break;
                        case 1:
                            id->MOVE = true;
                            id->SINGLE_COMMAND = true;
                            break;
                        case 2:
                            id->LEFT = true;
                            id->SINGLE_COMMAND = true;
                            break;
                        case 3:
                            id->RIGHT = true;
                            id->SINGLE_COMMAND = true;
                            break;
                        case 4:
                            id->REPORT = true;
                            id->SINGLE_COMMAND = true;
                            break;
                        case 5:
                            id->END = true;
                            id->SINGLE_COMMAND = true;
                    }
                }
            }

            bool success = ParseDirection(id->DIRECTION, last_arg);

            if (!success && id->SINGLE_COMMAND) {
                return true;
            } else if (success && id->PLACE) {
                return true;
            }
        }
    }
    return false;
}

bool ValidateCommand(command_id id, tabletop_id t) {
    if (id != NULL && t != NULL) {
        if (id->X_POS < 0 || id->Y_POS < 0 || id->X_POS >= t->max_x || id->Y_POS >= t->max_y) {
            return false;
        } else if ((id->RIGHT || id->LEFT || id->MOVE || id->REPORT || id->END) && id->SINGLE_COMMAND){
            return true;
        } else if (id->PLACE && (id->DIRECTION->west || id->DIRECTION->south || id->DIRECTION->east || id->DIRECTION->north)) {
            return true;
        }
    }
    return false;
}

bool ResetCommand(command_id id) {
    if (id != NULL) {
        id->SINGLE_COMMAND = false;
        id->LEFT = false;
        id->RIGHT = false;
        id->MOVE = false;
        id->REPORT = false;
        id->PLACE = false;
        id->END = false;
        id->X_POS = 0;
        id->Y_POS = 0;
        bool success = ResetDirection(id->DIRECTION);
        return success;
    }
    return false;
}

void DestroyCommand(command_id id) {
    if (id != NULL) {
        free(id);
    }
}
