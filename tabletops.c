//
//  tabletops.c
//  toyrobotsimulator
//
//  Created by Saujan Thapa on 24/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#include "tabletops.h"

#include <stdlib.h>


tabletop_id CreateTabletop(int table_length) {
    tabletop_id  id = malloc(sizeof(tabletop_t));

    if (id != NULL) {
        if (table_length <= 0 || table_length > MAX_TABLETOP_LENGTH) {
            id->max_x = DEFAULT_TABLETOP_LENGTH;
            id->max_y = DEFAULT_TABLETOP_LENGTH;
        }else{
            id->max_x = table_length;
            id->max_y = table_length;
        }
    }
    return id;
}

bool ResetTabletop(tabletop_id id, int table_length)
{
    if (id != NULL) {
        if (table_length <= 0 || table_length > MAX_TABLETOP_LENGTH) {
            id->max_x = DEFAULT_TABLETOP_LENGTH;
            id->max_y = DEFAULT_TABLETOP_LENGTH;
        }else{
            id->max_x = table_length;
            id->max_y = table_length;
        }
        return true;
    }
    return false;
}

void DestroyTabletop(tabletop_id id)
{
    if (id != NULL) {
        free(id);
    }
}