//
//  directions.h
//  toyrobotsimulator
//
//  Created by Saujan Thapa on 24/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#ifndef directions_h
#define directions_h

#include <stdbool.h>
#include <stdlib.h>
#include "tabletops.h"


/*
 * Data structure used to manage a robots' facing direction.
 *
 * Members:
 *      direction_id: The unique Id of the robots facing direction.
 *      north: Sets to true if robot is facing north and false otherwise.
 *      south: Sets to true if robot is facing south and false otherwise.
 *      east: Sets to true if robot is facing east and false otherwise.
 *      west: Sets to true if robot is facing west and false otherwise.
 *      name: Used to store the facing direction in words.
 */
typedef struct direction{
    bool north;
    bool south;
    bool east;
    bool west;
    char name[6];
}direction_t;

typedef direction_t * direction_id;

/*
 * CreateDirection:
 *
 * Initialises a direction to default variable.
 *
 * Input:
 *      N\A
 *
 * Output:
 *      Returns the address of an initialised direction object.
 */
direction_id CreateDirection();

/*
 * MOVE:
 *
 * Increments or Decrements the x or y values according to where
 * facing direction is and also according to the tabletop dimension.
 *
 * Input:
 *      id: Address of a declared direction.
 *      t: Address of a declared tabletop.
 *      x: Address of x coordinate where resulting x position should be stored.
 *      y: Address of y coordinate where resulting y position should be stored.
 *
 * Output:
 *      N\A
 */
void MOVE(direction_id id, tabletop_id t, int *x, int *y);

/*
 * LEFT:
 *
 * Sets the resulting facing direction when robot is rotated by +90 degrees.
 *
 * Input:
 *      id: Address of a declared direction.
 *
 * Output:
 *      N\A
 */
void LEFT(direction_id id);

/*
 * RIGHT:
 *
 * Sets the resulting facing direction when a robot is rotated by -90 degrees.
 *
 * Input:
 *      id: Address of a declared direction.
 *
 * Output:
 *      N\A
 */
void RIGHT(direction_id id);

/*
 * ScanDirection:
 *
 * Sets the facing direction of a robot in words.
 *
 * Input:
 *      id: Address of a declared direction.
 *
 * Output:
 *      N\A
 */
void ScanDirection(direction_id id);

/*
 * ParseDirection:
 *
 * Parses string in direction object.
 *
 * Input:
 *      id: Address of a declared direction.
 *      str: raw direction in the format of string.
 *
 * Output:
 *      Returns true if successful and false otherwise.
 */
bool ParseDirection(direction_id id, char * str);

/*
 * ResetDirection:
 *
 * Resets the direction.
 *
 * Input:
 *      id: Address of a declared direction.
 *
 * Output:
 *      Returns true if successful and false otherwise.
 */
bool ResetDirection(direction_id id);

/*
 * Releases the memory resources by the direction.
 */
void DestroyDirection(direction_id id);

#endif /* directions_h */
