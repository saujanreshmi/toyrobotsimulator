//
//  robots.h
//  toyrobotsimulator
//
//  Created by Saujan Thapa on 24/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#ifndef robots_h
#define robots_h

#include <stdio.h>

#include "tabletops.h"
#include "directions.h"
#include "commands.h"

/*
 * Data structure used to manage robots.
 *
 * Members:
 *      robot_id: The unique Id of the robot.
 *      tabletop: Dimension of the robots moving area.
 *      x,y: The location of the robot, represented as int.
 *      direction: Facing direction of the robot.
 *      command: Buffer to store command for the robot.
 */
typedef struct robot{
    tabletop_id tabletop;
    int x;
    int y;
    direction_id direction;
    command_id command;
}robot_t;

typedef robot_t * robot_id;

/*
 * This function never to be called directly instead call CreateRobot;
 * _createRobotInternal:
 *
 * Initialise a robot to default variable.
 *
 * Input:
 *      table_length: Length of the square tabletop where robot will be moving. Maximum dimension = 255 x 255;
 *            If 0 or less than 0 or more than 255 is passed than default dimension of 5 x 5 will be picked.
 *
 * Output:
 *      Returns the address of an initialised robot object.
 */
robot_id _createRobotInternal(int tabletop_length);

/*
 * CreateRobot:
 *
 * MACRO definition to call CreateRobotInternal
 * This macros' parameter is optional
 * if this macro is called without parameter than CreateRobotIntenal function is called with parameter 0
 */
#define CREATE_1(tabletop_length) _createRobotInternal(tabletop_length)
#define CREATE_0() CREATE_1(0)

#define FUNC_CHOOSER(_f1, _f2, ...) _f2
#define FUNC_RECOMPOSER(argsWithParentheses) FUNC_CHOOSER argsWithParentheses
#define CHOOSE_FROM_ARG_COUNT(...) FUNC_RECOMPOSER((__VA_ARGS__, CREATE_1, ))
#define NO_ARG_EXPANDER() ,CREATE_0
#define MACRO_CHOOSER(...) CHOOSE_FROM_ARG_COUNT(NO_ARG_EXPANDER __VA_ARGS__ ())
#define CreateRobot(...) MACRO_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

/*
 * ExecuteRobot:
 *
 * Executes provided command to the robot if valid.
 *
 * Calls DestroyRobot if 'END' command is supplied
 *
 * Input:
 *      robot: Address of a declared robot.
 *      cmd: Robot command in the form of string.
 *
 * Output:
 *      Returns true if command is successfully executed and false otherwise
 */
bool ExecuteRobotCommand(robot_id id, char * cmd, char * feedback, int feedack_length);

/*
 * This function never to be called directly instead call ResetRobot;
 * _resetRobotInternal:
 *
 * Resets the robot in new table.
 *
 * Input:
 *      id: Address of a declared robot.
 *      tabletop_length: Length of the square tabletop where robot will be moving. Maximum = 255 x 255;
 *            If 0 or less than 0 is passed for any of the dimension than default value of 5 x 5 will be picked.
 *
 * Output:
 *      Returns true if successful and false otherwise.
 */
bool _resetRobotInternal(robot_id id, int tabletop_length);

/*
 * ResetRobot:
 * MACRO definition to call ResetRobotInternal
 * This macros' second parameter is optional
 * if this macro is called without second parameter than CreateRobotIntenal function is called with second parameter 0
 */
#define RESET_2(id, tabletop_length) _resetRobotInternal(id, tabletop_length)
#define RESET_1(id) RESET_2(id, 0)

#define FUNC_CHOOSER1(_f1, _f2, _f3, ...) _f3
#define MACRO_CHOOSER1(...) FUNC_CHOOSER1(__VA_ARGS__, RESET_2, RESET_1, )
#define ResetRobot(...) MACRO_CHOOSER1(__VA_ARGS__)(__VA_ARGS__)

/*
 * DestroyRobot:
 *
 * Releases the memory resources used by the robot.
 */
void DestroyRobot(robot_id id);

#endif /* robots_h */
