//
//  robots.c
//  toyrobotsimulator
//
//  Created by Saujan Thapa on 24/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//
#include <string.h>

#include "robots.h"

robot_id _createRobotInternal(int table_length) {
    robot_id id = malloc(sizeof(robot_t));
    if (id != NULL) {
        id->command = CreateCommand();
        id->direction = CreateDirection();
        id->tabletop = CreateTabletop(table_length);
        id->x = -1;
        id->y = -1;
        if (id->command == NULL || id->direction == NULL || id->tabletop == NULL) {
            return NULL;
        }
    }
    return id;
}

bool ExecuteRobotCommand(robot_id id, char * cmd, char * feedback, int feedback_length) {
    if (id != NULL && cmd != NULL && feedback_length >= 50) {
        memset(feedback, '\0', feedback_length);
        bool pc = ParseCommand(id->command, cmd);
        if (!pc) {
            strcpy(feedback, "[ERROR] Failed to parse the command or command is invalid");
            return false;
        }
        bool vc = ValidateCommand(id->command, id->tabletop);
        if (!vc) {
            strcpy(feedback, "[ERROR] Command is not valid");
            return false;
        }
        if (id->command->PLACE) {
            id->x = id->command->X_POS;
            id->y = id->command->Y_POS;
            id->direction->east = id->command->DIRECTION->east;
            id->direction->west = id->command->DIRECTION->west;
            id->direction->north = id->command->DIRECTION->north;
            id->direction->south = id->command->DIRECTION->south;
            return true;
        } else if (id->command->REPORT && (id->x != -1 && id->y != -1)) {
            ScanDirection(id->direction);
            memset(feedback,'\0',50);
            sprintf(feedback,"Output: %d,%d,%s",id->x,id->y,id->direction->name);
            return true;
        } else if ((id->command->MOVE || id->command->RIGHT || id->command->LEFT) && (id->x != -1 && id->y != -1)) {
            if (id->command->MOVE) {
                MOVE(id->direction, id->tabletop, &(id->x), &(id->y));
                return true;
            } else if (id->command->LEFT) {
                LEFT(id->direction);
                return true;
            } else if(id->command->RIGHT) {
                RIGHT(id->direction);
                return true;
            }
        } else if (id->command->END) {
            DestroyRobot(id);
            strcpy(feedback,"Robot is now destroyed. Exiting...");
            return true;
        }
    }
    return false;
}

bool _resetRobotInternal(robot_id id, int tabletop_length) {
    if (id != NULL) {
        bool rt = ResetTabletop(id->tabletop, tabletop_length);
        bool rc = ResetCommand(id->command);
        bool rd = ResetDirection(id->direction);
        id->x = -1;
        id->y = -1;
        if (rt && rc && rd) {
            return true;
        }
    }
    return false;
}

void DestroyRobot(robot_id id) {
    if (id != NULL) {
        DestroyTabletop(id->tabletop);
        DestroyDirection(id->direction);
        DestroyCommand(id->command);
        free(id);
    }
}


