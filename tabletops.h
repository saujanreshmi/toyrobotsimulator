//
//  tabletops.h
//  toyrobotsimulator
//
//  Created by Saujan Thapa on 24/6/19.
//  Copyright © 2019 Saujan Thapa. All rights reserved.
//

#ifndef tabletops_h
#define tabletops_h

#include <stdio.h>
#include <stdbool.h>

#define DEFAULT_TABLETOP_LENGTH 5
#define MAX_TABLETOP_LENGTH 255

/*
 * Data structure used to manage a robots' tabletops.
 *
 * Members:
 *      tabletop_id: The unique Id of the tabletop.
 *      max_x: Total number of coordinates in x-axis SOUTH to NORTH
 *      max_y: Total number of coordinates in y-axis WEST to EAST
 */
typedef struct tabletop{
    int max_x;
    int max_y;
}tabletop_t;

typedef tabletop_t * tabletop_id;

/*
 * CreateTabletop:
 *
 * Initialises a tabletop to default variable.
 *
 * Input:
 *      table_length: Length of the square tabletop. Maximum dimension = 255 x 255;
 *            If 0 or less than 0 or more than 255 is passed than default dimension of 5 x 5 will be picked.
 *
 * Output:
 *      Returns the address of an initialised tabletop object.
 */
tabletop_id CreateTabletop(int table_length);

/*
 * ResetTabletop:
 * Resets the tabletop.
 *
 * Input:
 *      id: Address of a declared tabletop.
 *      table_length: Length of the square tabletop. Maximum dimension = 255 x 255;
 *            If 0 or less than 0 or more than 255 is passed than default dimension of 5 x 5 will be picked.
 *
 * Output:
 *      Returns true if successful and false otherwise.
 */
bool ResetTabletop(tabletop_id id, int table_length);

/*
 * DestroyTabletop:
 *
 * Releases the memory resources used by the tabletop.
 */
void DestroyTabletop(tabletop_id id);

#endif /* tabletops_h */
