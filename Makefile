#
#  Makefile
#  toyrobotsimulator
#
#  Created by Saujan Thapa on 24/6/19.
#  Copyright © 2019 Saujan Thapa. All rights reserved.
#

CC=gcc
TARGET=libtoyrobotsimulator.a
FLAGS=-c -Wall -Werror -std=gnu99

all: $(TARGET)

$(TARGET): tabletops.o directions.o commands.o robots.o
	ar r $(TARGET) tabletops.o directions.o commands.o robots.o

tabletops.o: tabletops.h
	@echo "Compiling with..."
	@$(CC) -v
	$(CC) $(FLAGS) -o tabletops.o -I include tabletops.c

directions.o: directions.h
	$(CC) $(FLAGS) -o directions.o -I include directions.c

commands.o: commands.h directions.h
	$(CC) $(FLAGS) -o commands.o -I include commands.c

robots.o: robots.h commands.h directions.h
	$(CC) $(FLAGS) -o robots.o -I include robots.c

clean:
	@echo "Cleaning compiled and archieved files..."
	rm $(TARGET)
	rm *.o

